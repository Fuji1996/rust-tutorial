// Crates : Modules that produce a library or executable
// Modules : Organize and handle privacy
// Packages : Build, test and share crates
// Paths : A way of naming an item such as struct, function
// packages can contain 0 or 1 library crates, they can also create as many 
// binary crates ( in /bin folder ) as you want 
// everything is private by default, have to declare public explicitly if needed
mod pizza_order{
    
    pub struct Pizza{
        pub dough: String,
        pub cheese: String,
        pub topping: String,
    }
 
    impl Pizza {
        pub fn lunch(input_topping: &str) -> Pizza{
            Pizza{
                dough: String::from("regular dough"),
                cheese: String::from("regular cheese"),
                topping: String::from(input_topping),
            }
        }
    }

    pub mod help_customer{
        fn seat_at_table(){
            println!("customer seated");
        }    
        pub fn take_order(){
            seat_at_table();
            let cust_pizza: super::Pizza = super::Pizza::lunch("veggie");
            serve_customer(cust_pizza);
        }
        fn serve_customer(custom_pizza: super::Pizza){
            println!("the customer is served with regular pizza with {} toppings",
             custom_pizza.topping);
        }
    }
}

pub fn order_food(){
    crate::restaurant::pizza_order::help_customer::take_order();
}