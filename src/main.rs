#![allow(unused)]

use std::io::{self, stdin};
use std::process::Output;
use rand::Rng;
use std::io::{Write, BufReader, BufRead, ErrorKind};
use std::fs::File;
use std::cmp::Ordering;

use std::ops::Add;
use std::collections::HashMap;

mod restaurant;
use crate::restaurant::order_food;

fn main() {
    // order_food(); // function from diiferent module::crate
    
    closure_op();
    
}

fn smart_pointer_op(){
    // pointer is just an address to location in memory

}

fn closure_op(){
    // closure basically is a function without a name and more than likely going to be stored 
    // in a variable and they can be used to pass a function into another function
    // let var_name = | parameters | -> return_type { BODY };
    let can_vote = |age: i32 | {
        age >= 18
    };
    println!("Can vote : {}",can_vote(8));

    // closure can access variable outside of it;s body unlike funtions whenever 
    // it is borrowing
    let mut samp1 = 5;
    let print_var = || println!("samp1 = {}", samp1);
    print_var();

    // we can change value if we make closure mutable
    samp1 = 10;
    let mut change_var = || samp1 += 1;
    change_var();
    println!("samp1 = {}", samp1);
    samp1 = 10;
    println!("samp1 = {}", samp1);

    // we can pass closure to functions
    // yes, we can define functions within functions too
    fn use_func<T>(a: i32, b: i32, func: T) -> i32
    where T: Fn(i32, i32) -> i32 {
        func(a, b)
    }
    let sum = |a, b| a+b;
    let prod = |a, b| a*b;
    println!("3 + 4 = {}", use_func(3, 4, sum));
    println!("3 * 4 = {}", use_func(3, 4, prod));


}

fn iterator_op(){
    let mut arr_it = [1,2,3,4];
    for val in arr_it.iter(){
        // here we are iterating by borrowing the array value, 
        // so we won't be able to change the array value here
        println!("{}", val);
    }

    // using 'arr_it.into_iter()' will consume the collection 
    // and we will not be able to use the collection

    let mut iter1 = arr_it.iter();
    println!("1st : {:?}", iter1.next());
    // as now iter1 is mutable we can loop through array by using 'iter1.next()'

}

fn error_handling_panic(){
    // panic!() macro prints error, clean up memory, shut down
    // let lil_arr = [1, 2];
    // println!("{}", lil_arr[10]);

    // 1
    let path = "lines.txt";
    let output = File::create(path);
    // Result has 2 varients 'Ok' and 'Err'
    // enum Result<T,E>{
    //  Ok(T),
    //  Err(E),
    //}
    // here T represents the data type of the value returns and E the type of error
    let mut output = match output {
        Ok(file) => file,
        Err(error) => panic!("Problem creating file : {:?}", error),
    };

    // 2 
    write!(output, "Just some \n Random words").expect("Failed to write to file");

    // 3
    let input = File::open(path).unwrap(); 
    // unwrap basically ignores the result and gives us the output
    // another way of going around Result
    let buffered = BufReader::new(input);
    for line in buffered.lines(){
        println!("{}", line.unwrap());
    }

    // 4
    // catching specific errors
    let output2 = File::create("random.txt");
    let output2 = match(output2) {
        Ok(file) => file,
        Err(error) => match error.kind(){
            ErrorKind::NotFound => match File::create("rand.txt"){
                Ok(fc) => fc,
                Err(e) => panic!("can't create file : {:?}", e),
            },
            _other_error => panic!("problem opening file : {:?}", error),
        }
    };

}

fn struct_op(){
    struct Customer{
        name: String,
        address: String,
        balance: f32,
    }

    let mut fuji = Customer{
        name: String::from("fuji"),
        address: String::from("adarsha nagar"),
        balance: 230.50,
    };
    println!("name: {}, address: {}, balance: {}", fuji.name,
     fuji.address, fuji.balance);
    fuji.address = String::from("badda");
    println!("name: {}, address: {}, balance: {}", fuji.name,
     fuji.address, fuji.balance);
}
fn struct_generic_op(){
    struct Rectangl<T, U>{
        length: T,
        width: U,
    }
    let rec: Rectangl<i32, f64> = Rectangl { length: 10, width: 10.5 };
    println!("len: {}, wid: {}",rec.length, rec.width);
}
fn struct_traits_op(){
    const PI: f32 = 3.1416;

    trait Shape {
         fn new(length: f32, width: f32) -> Self;
         fn area(&self) -> f32;
    }

    struct Rectangle{length:f32, width: f32};
    struct Circle{length:f32, width: f32};

    impl Shape for Rectangle {
        fn new(length: f32, width: f32) -> Rectangle{
            return Rectangle{length, width};
        }
        fn area(&self) -> f32{
            return self.length * self.width;
        }
    }
    impl Shape for Circle {
        fn new(length: f32, width: f32) -> Circle{
            return Circle{length, width};
        }
        fn area(&self) -> f32{
            return (self.length/2.0).powf(2.0)* PI;
        }
    }
    
    let rec: Rectangle = Shape::new( 10.0, 10.0);
    let cir: Circle = Shape::new( 10.0, 10.0);
    println!("Rec area : {}", rec.area());
    println!("Cir area : {}", cir.area());
}

fn struct_traits_op_2(){
    const PI: f32 = 3.1416;

    trait Shape {
         fn new(x: f32, y: f32) -> Self;
         fn area(&self) -> f32;
    }

    struct Rectangle{length:f32, width: f32};
    struct Circle{radius: f32};

    impl Shape for Rectangle {
        fn new(a: f32, b: f32) -> Rectangle{
            return Rectangle{length:a, width:b};
        }
        fn area(&self) -> f32{
            return self.length * self.width;
        }
    }
    impl Shape for Circle {
        fn new(a: f32, b: f32) -> Circle{
            // ignoring 2nd parameter for Circle
            return Circle{radius: a};
        }
        fn area(&self) -> f32{
            return (self.radius/2.0).powf(2.0)* PI;
        }
    }
    
    let rec: Rectangle = Shape::new( 10.0, 10.0);
    let cir: Circle = Shape::new( 10.0, 10.0);
    println!("Rec area : {}", rec.area());
    println!("Cir area : {}", cir.area());
}

fn hashmap_op(){
    let mut heroes = HashMap::new();
    heroes.insert("superman", "clark kent");
    heroes.insert("batman", "bruce wayne");
    heroes.insert("flash","barry allen");

    for(k,v) in heroes.iter(){
        println!("( key : {}, value : {} )",k,v);
    }
    println!("hashmap len : {}", heroes.len());

    if(heroes.contains_key(&"batman")){
        let the_batman = heroes.get(&"batman");
        match the_batman{
            Some(x) => println!("batman is a hero"),
            None => println!("batman is not a hero"),
        }
    }
}

fn string_test(){
    let str1: String = String::from(" world");
    let str2 = str1.clone();
    println!("str1 : {}, str2 : {}", str1, str2);
    let str3 = print_return_str(str1);
    println!("str2 : {}, str3 = {}", str2, str3);
    // here str1 no longer exist as the ownership goes to str3 
    // we could use &str1 to pass in print_return_str() to give str3 the borrowed value of str1

    let mut str4 = String::from("Fuji");
    println!("str4 (before change) : {}", str4);
    change_string(&mut str4);
    println!("str4 (after change) : {}", str4);
    println!("",);

}
fn print_str(x: String) {
    println!("A string : {}", x);
}
fn print_return_str(x: String) -> String {
    println!("A string : {}", x);
    x
}
fn change_string(name: &mut String){
    name.push_str(" is happy");
    println!("changed string : {}", name);
} 

fn ownership(){
    let str1 = String::from("world");
    // let str2 = str1; //ownership moves to str2, str1 no longer exists
    // println!("hello {}",str1); // will throw an error

    let str2 = str1.clone(); // making 2 copies of str1
    println!("hello {}",str1);
}

fn get_sum_gen<T: Add<Output = T>>(x:T, y:T) -> T {
    return x + y;
}
fn print_generic(){
    println!("5 + 4 = {}", get_sum_gen(5, 4));
    println!("5.2 + 4.6 = {}", get_sum_gen(5.2, 4.6));
}

fn sum_list(list :&[i32]) -> i32{
    let mut sum = 0;
    for &val in list.iter() {
        sum += &val;
    }
    return sum;
}
fn print_list_sum(){
    let num_list = vec![1,2,3,4,5];
    println!("sum of the list : {}", sum_list(&num_list));
}

fn get_sum_multiple(){
    let (val_1, val_2) = get_sum_3(3);
    println!("val_1 : {}, val_2 : {}", val_1, val_2);
}
fn get_sum_3(x: i32) -> (i32, i32){
    return (x+1,x+2);
}
fn get_sum_2(x: i32, y: i32) -> i32{
    x+y  // or 'return x + y;' is same
}
fn get_sum(x: i32, y: i32){
     println!("{} + {} = {}", x, y,x+y);
}

fn vector_op(){
    let vect1: Vec<i32> = Vec::new();
    let mut vec2 = vec![1,2,3,4];
    vec2.push(5);
    println!("vec[4] : {:?}, len : {}", vec2[4], vec2.len());

    let second = &vec2[1];
    let third = &vec2[2];
    println!("vec[1] : {}",second); 
    match vec2.get(1){
        Some(second) => println!("2nd : {}",second), //what is happening here?
        None => println!("No second value"),
    }

    for i in &mut vec2{
        *i = *i * 2;
    }


    for i in &vec2{
        println!("{}", i);
    }

    println!("vec len : {}", vec2.len());
    println!("Pop : {:?}", vec2.pop());
    println!("Pop : {:?}", vec2.pop());
    for i in &vec2{
        println!("{}", i);
    }


}

fn enum_op(){
    enum Day{
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }

    impl Day{
        fn is_weekend(&self) -> bool{
            match self{
                Day::Saturday | Day::Sunday => true,
                _ => false,
            }
        }
    }
    let today: Day = Day::Monday;
    match today{
        Day::Monday => println!("hateful monday"),
        Day::Tuesday => println!("boring tuesday"),
        Day::Wednesday => println!("hopeful wednesday"),
        Day::Thursday => println!("waiting thursday"),
        Day::Friday => println!("exciting friday"),
        Day::Saturday => println!("chill saturday"),
        Day::Sunday => println!("planning sunday"),
    }

    println!("is today the weekend {}", today.is_weekend());

}

fn casting_op(){
     let int_u8: u8 = 5;
     let int2_u8: u8 = 4;
     let int3_u32: u32 = int_u8 as u32 + int2_u8 as u32;
     println!("int3_u32: {:?}", int3_u32);
}

fn string_op(){
    let mut str1 = String::new();
    str1.push('A');
    str1.push_str(" word");
    for word in str1.split_whitespace() {
        println!("{}",word);
    }
    let str2 = str1.replace("A", "Another");
    println!("{}",str2);

    let str3 = String::from("z d h t e e m a b");
    println!("string : {}",str3);
    let mut v1: Vec<char> = str3.chars().collect();
    v1.sort();
    v1.dedup();
    for c in v1{
        println!("char : {}",c);
    }
    let str4 = "random string";
    let mut str5 = str4.to_string();
    let byte_arr1 = str5.as_bytes();
    for b in byte_arr1{
        println!("byte : {}",b);
    }
    let str6 = &str5[0..5];
    println!("{}'s length is {}", str6, str6.len());
    str5.clear();

    let str6 = String::from("just some");
    let str7 = String::from(" words");
    let str8 = str6 + &str7; // str6 is not usable and str7 is usable after this line
    println!("str8 : {}",str8);
    //println!("str6 : {}, str7 : {}", str6, str7); //str6 is not usable as it moved inside str8


}

fn tuple_op(){
    let my_tuple: ( u8, String, f64 ) = (47, "Fuji".to_string(), 50_000.00);

    println!("Name : {}", my_tuple.1);
    let ( v1, v2, v3 ) = my_tuple;
    println!("Age : {}", v1);

}

fn loop_op(){
    let arr2 = [1,2,3,4,5,6,7,8,9];
    let mut loop_idx = 0;
    loop{
        if arr2[loop_idx] % 2 == 0 {
            loop_idx += 1;
            continue;
        }

        if arr2[loop_idx] == 9 {
            break;
        }

        println!("odd number : {:?}", arr2[loop_idx]);
        loop_idx += 1;
    }
    loop_idx = 0;

    while loop_idx < arr2.len(){
        println!("arr2[{}] : {}", loop_idx, arr2[loop_idx]);
        loop_idx += 1;
    }

    for val in arr2.iter(){
        println!("val : {}", val);
    }


}

fn array_op(){
    let arr_1 = [1,2,3,4];
    println!("1st element: {:?}", arr_1[0]);
    println!("length : {}", arr_1.len());
}

fn ternary_operator(){
    let my_age = 26;
    let can_vote = if my_age >= 18 {
        true
    }else{
        false
    };

    println!("can_vote: {}", can_vote);
}

fn match_operator(){
    let age = 50;
    match age {
        1..=18 => println!("important birthday"),
        21 | 50 => println!("important birthday"),
        65..=i32::MAX => println!("important birthday"),
        _ => println!("not an important birthday"),
    };

    let my_age = 18;
    let voting_age = 18;
    match  my_age.cmp(&voting_age){
        Ordering::Less => println!("can't vote"),
        Ordering::Greater => println!("can vote"),
        Ordering::Equal => println!("Congrats.>! you have gained the right to vote..!"),
    };
}

fn if_else(){
    let age = 50;
    if ( age >= 1 ) && ( age <= 18 ) {
        println!("important birthday");
    }else if ( age == 21 ) || ( age == 50 ) {
        println!("important birthday");
    }else if age >= 65 {
        println!("important birthday");
    }else{
        println!(" not an important birthday");
    }
}

fn random_generator(){
    let random_num = rand::thread_rng().gen_range(1..101);
    println!("random num : {}", random_num);
}

fn operations(){
    let mut num_3:u32 = 5;
    let num_4:u32 = 4;

    println!("5 + 4 = {}", num_3 + num_4);
    println!("5 - 4 = {}", num_3 - num_4);
    println!("5 * 4 = {}", num_3 * num_4);
    println!("5 / 4 = {}", num_3 / num_4);
    println!("5 % 4 = {}", num_3 % num_4);

    num_3 += 1;

}

fn input(){
    print!("what is your name?\n");
    let mut name = String::new();
    let greeting = "nice to meet you";
    io::stdin().read_line(&mut name).expect("didn't receive input");
    println!("Hello {} !, {}", name.trim_end(), greeting);
}

fn variables(){
    const ONE_MIL:u32 = 1_000_000;
    const PI: f32 = 3.14159265;
    let age = "26";
    let mut age: u32 = age.trim().parse().expect("not assigninable");
    age = age + 1;
    println!("I am {} and I want ${}",age, ONE_MIL);
}

fn data_types(){
    println!("max u8 {}, min u8 {}", u8::MAX, u8::MIN);
    println!("max u16 {}, min u8 {}", u16::MAX, u16::MIN);
    println!("max u32 {}, min u8 {}", u32::MAX, u32::MIN);
    println!("max u64 {}, min u8 {}", u64::MAX, u64::MIN);
    println!("max u128 {}, min u128 {}", u128::MAX, u128::MIN);
    println!("max usize {}, min usize {}", usize::MAX, usize::MIN);
    println!("max f32 {}, min f32 {}", f32::MAX, f32::MIN);
    // println!("max f64 {}, min f64 {}", f64::MAX, f64::MIN);

    let is_true = true;
    let my_grade = 'A';

    // f32 has 6 digit precission
    let num_1 : f32 = 1.111111111111111;
    println!("f32 : {}", num_1+0.111111111111111);

    // f64 has 14 digit precission
    let num_2 : f64 = 1.111111111111111;
    println!("f64 : {}", num_2+0.111111111111111);

}
